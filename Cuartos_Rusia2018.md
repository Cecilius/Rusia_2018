# Cuartos de Final

### Llave 1
|Bandera               |      Equipos         | Goles|
|----------------------|----------------------|-------|    
|<img alt="Uruguay" src="http://flags.fmcdn.net/data/flags/w580/uy.png" width="50" height="50">| Uruguay | 0| 
|<img alt="Francia" src="http://flags.fmcdn.net/data/flags/w580/fr.png" width="50" height="50">| Francia|2|

___
### Llave 2
|Bandera               |      Equipos         | Goles|
|----------------------|----------------------|-------|
| <img alt="Croacia" src="http://flags.fmcdn.net/data/flags/w580/hr.png" width="50" height="50">| Croacia|2 (3)*|  
| <img alt="Rusia" src="http://flags.fmcdn.net/data/flags/w580/ru.png" width="50" height="50">| Rusia |2 (4)*|
        

___
### Llave 3
|Bandera               |      Equipos         | Goles|
|----------------------|----------------------|-------|
| <img alt="Brasil" src="http://flags.fmcdn.net/data/flags/w580/br.png" width="50" height="50">| Brasil|1|
| <img alt="Bélgica" src="http://flags.fmcdn.net/data/flags/w580/be.png" width="50" height="50">| Bélgica|2|

     

___
### Llave 4

|Bandera               |      Equipos         | Goles|
|----------------------|----------------------|-------|
| <img alt="Suecia" src="http://flags.fmcdn.net/data/flags/w580/se.png" width="50" height="50">| Suecia |0|
| <img alt="Inglaterra" src="https://images.ecosia.org/YhP6-1HZWYPeetlZmdY_IFiosBQ=/0x390/smart/http%3A%2F%2F3.bp.blogspot.com%2F-ZPZYb1wmnG8%2FUE1Uu8_U4YI%2FAAAAAAAAANE%2FDIJad4t4cBQ%2Fs1600%2Fengland_flag_pic.jpg" width="50" height="50">| Inglaterra |2|

  
___


* Tanda de penales*

## Desarrollado:

- [Cecilio Niño](https://twitter.com/Cecilio_25)
