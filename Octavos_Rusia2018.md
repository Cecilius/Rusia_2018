# Octavos de Final

### Llave 1
|Bandera               |      Equipos         | Goles|
|----------------------|----------------------|-------|    
|<img alt="Uruguay" src="http://flags.fmcdn.net/data/flags/w580/uy.png" width="50" height="50">| Uruguay | 2| 
|<img alt="Portugal" src="http://flags.fmcdn.net/data/flags/w580/pt.png" width="50" height="50">| Portugal | 1|


 
___
### Llave 2

|Bandera               |      Equipos         | Goles|               
|----------------------|----------------------|-------|              
|<img alt="España" src="http://flags.fmcdn.net/data/flags/w580/es.png" width="50" height="50">| España | 1 (3)*|
| <img alt="Rusia" src="http://flags.fmcdn.net/data/flags/w580/ru.png" width="50" height="50">| Rusia | 1 (4)*|

___
### Llave 3
|Bandera               |      Equipos         | Goles|
|----------------------|----------------------|-------|
| <img alt="Francia" src="http://flags.fmcdn.net/data/flags/w580/fr.png" width="50" height="50">| Francia|4|
| <img alt="Argentina" src="http://flags.fmcdn.net/data/flags/w580/ar.png" width="50" height="50">| Argentina |2|  
      

___
### Llave 4
|Bandera               |      Equipos         | Goles| 
|----------------------|----------------------|-------|
| <img alt="Croacia" src="http://flags.fmcdn.net/data/flags/w580/hr.png" width="50" height="50">| Croacia|1 (3)*|      
| <img alt="Dinamarca" src="http://flags.fmcdn.net/data/flags/w580/dk.png" width="50" height="50">| Dinamarca |1 (2)* |    

___
### Llave 5
|Bandera               |      Equipos         | Goles|
|----------------------|----------------------|-------|
| <img alt="Brasil" src="http://flags.fmcdn.net/data/flags/w580/br.png" width="50" height="50">| Brasil|2| 
| <img alt="México" src="http://flags.fmcdn.net/data/flags/w580/mx.png" width="50" height="50">| México | 0|
     

___
### Llave 6

|Bandera               |      Equipos         | Goles|
|----------------------|----------------------|-------|
| <img alt="Suecia" src="http://flags.fmcdn.net/data/flags/w580/se.png" width="50" height="50">| Suecia | 1|
| <img alt="Suiza" src="http://flags.fmcdn.net/data/flags/w580/ch.png" width="50" height="50">| Suiza |0|  
___


### Llave 7

|Bandera               |      Equipos         | Goles|
|----------------------|----------------------|-------|
| <img alt="Bélgica" src="http://flags.fmcdn.net/data/flags/w580/be.png" width="50" height="50">| Bélgica|3|
| <img alt="Japón" src="http://flags.fmcdn.net/data/flags/w580/jp.png" width="50" height="50">| Japón | 2|
___


### Llave 8

|Bandera               |      Equipos         | Goles|
|----------------------|----------------------|-------|
|<img alt="Colombia" src="http://flags.fmcdn.net/data/flags/w580/co.png" width="50" height="50">| Colombia | 1 (3)*|
|<img alt="Inglaterra" src="https://images.ecosia.org/YhP6-1HZWYPeetlZmdY_IFiosBQ=/0x390/smart/http%3A%2F%2F3.bp.blogspot.com%2F-ZPZYb1wmnG8%2FUE1Uu8_U4YI%2FAAAAAAAAANE%2FDIJad4t4cBQ%2Fs1600%2Fengland_flag_pic.jpg" width="50" height="50">| Inglaterra |1 (4)*|
___
* Tanda de penales*
## Desarrollado:

- [Cecilio Niño](https://twitter.com/Cecilio_25)
